import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_spinning_wheel/flutter_spinning_wheel.dart';
import 'dart:math';

import 'package:spin_wheel/spin_wheel_duplicate.dart';
import 'package:spin_wheel/spin_wheel_with_button.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      // home: HomePage(),
      home: SpinWheelByHand(),
    );
  }
}

class SpinWheelByHand extends StatefulWidget {
  @override
  _SpinWheelByHandState createState() => _SpinWheelByHandState();
}

class _SpinWheelByHandState extends State<SpinWheelByHand> {
  List<Luck> _items = [
    Luck("Tất cả nam uống", Colors.accents[0]),
    Luck("Uống 50%", Colors.accents[2]),
    Luck("Bỏ lượt", Colors.accents[4]),
    Luck("Tất cả nữ uống", Colors.accents[6]),
    Luck("Bên trái uống", Colors.accents[8]),
    Luck("Chỉ định một người uống", Colors.accents[10]),
    Luck("Nêu cảm nhận về một người", Colors.accents[12]),
    Luck("Uông 100%", Colors.accents[14]),
  ];

  final StreamController _dividerController = StreamController<int>();

  double _generateRandomAngle() => Random().nextDouble() * pi * 2;

  double _rotote(int index) => (index / _items.length) * 2 * pi;

  @override
  void dispose() {
    _dividerController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.deepPurpleAccent,
      body: Column(
        children: [
          const SizedBox(height: 100,),
          StreamBuilder(
            stream: _dividerController.stream,
            builder: (context, snapshot){
              return snapshot.hasData ? RouletteScore(snapshot.data) : Container();
            },
          ),
          Expanded(
            child: Container(
              child: Center(
                child: SpinningWheelDuplicate(
                  spinWidget: Stack(
                    alignment: Alignment.center,
                    children: <Widget>[
                      for (var luck in _items) ...[_buildCard(luck)],
                      for (var luck in _items) ...[_buildText(luck)],
                    ],
                  ),
                  width: 400,
                  height: 400,
                  dividers: 8,
                  initialSpinAngle: _generateRandomAngle(),
                  // onEnd: _dividerController.add,
                  onUpdate: _dividerController.add,
                  onEnd: _dividerController.add,
                  secondaryImageHeight: 70,
                  secondaryImageWidth: 60,
                  secondaryWidget: Center(
                    child: Stack(
                      children: [
                        ClipPath(
                          clipper: _ArrowClipper(),
                          child: Container(
                            height: 70,
                            width: 60,
                            decoration: BoxDecoration(
                              color: Colors.black,
                              // shape: BoxShape.circle,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  canInteractWhileSpinning: false,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _buildCard(Luck luck) {
    var _rotate = _rotote(_items.indexOf(luck));
    var _angle = 2 * pi / _items.length;
    return Transform.rotate(
      angle: _rotate,
      child: ClipPath(
        clipper: _LuckPath(_angle),
        child: Container(
          height: 400,
          width: 400,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [luck.color, luck.color.withOpacity(0)])),
        ),
      ),
    );
  }

  _buildText(Luck luck) {
    var _rotate = _rotote(_items.indexOf(luck));
    return Transform.rotate(
      angle: _rotate,
      child: Material(
        color: Colors.transparent,
        child: Container(
          height: 400,
          width: 400,
          alignment: Alignment.topCenter,
          child: Container(
            child: Transform.rotate(
              angle: pi / 2,
              child: ConstrainedBox(
                constraints: BoxConstraints.expand(height: 400 / 3, width: 100),
                child: Center(child: Text(luck.text, style: TextStyle(color: Colors.black87),)),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _LuckPath extends CustomClipper<Path> {
  final double angle;

  _LuckPath(this.angle);

  @override
  Path getClip(Size size) {
    Path _path = Path();
    Offset _center = size.center(Offset.zero);
    Rect _rect = Rect.fromCircle(center: _center, radius: size.width / 2);
    _path.moveTo(_center.dx, _center.dy);
    _path.arcTo(_rect, -pi / 2 - angle / 2, angle, false);
    _path.close();
    return _path;
  }

  @override
  bool shouldReclip(_LuckPath oldClipper) {
    return angle != oldClipper.angle;
  }
}

class _ArrowClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path _path = Path();
    // _path.lineTo(0.0, 0.0);
    // _path.lineTo(size.width, 0.0);
    // _path.lineTo(size.width, size.height /2);
    // _path.lineTo(size.width / 2, size.height);
    // _path.lineTo(0, size.height/2);
    _path.moveTo(size.width/2 - 5, 10);
    _path.lineTo(size.width/2, 0);
    _path.lineTo(size.width/2 + 5, 10);
    _path.addOval(Rect.fromCircle(center: Offset( size.width/2, size.width/2 + 10), radius: size.width/2));
    _path.close();
    return _path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

class RouletteScore extends StatelessWidget {
  final int selected;

  final Map<int, String> labels = {
    1: 'Tất cả nam uống',
    2: 'Uống 50%',
    3: 'Bỏ lượt',
    4: 'Tất cả nữ uống',
    5: 'Bên trái uống',
    6: 'Chỉ định một người uống',
    7: 'Nêu cảm nhận về một người',
    8: 'Uông 100%',
  };

  RouletteScore(this.selected);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      margin: EdgeInsets.symmetric(horizontal: 20),
      color: Colors.black,
      child: Center(
        child: Text( labels[selected], style: TextStyle(
            color: Colors.white, fontSize: 18
        ), ),
      ),
    );
  }
}
